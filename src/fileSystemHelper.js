// @ts-nocheck
import {
  Plugins,
  FilesystemDirectory,
  FilesystemEncoding
} from "@capacitor/core";
const filename = "appdataq1/downloadQueue.txt";
import { StartDownloadQueue } from "./fileDownloader";
const { Filesystem } = Plugins;

export async function fileWrite(d) {
  console.log("Creating new file");
  const Stringified = JSON.stringify(d);
  try {
    await Filesystem.writeFile({
      path: filename,
      data: Stringified,
      directory: FilesystemDirectory.Documents,
      encoding: FilesystemEncoding.UTF8
    });
    console.log("Created new file and written data");
  } catch (e) {
    console.error("Unable to write file", e);
  }
}

export async function fileRead() {
  try {
    let contents = await Filesystem.readFile({
      path: filename,
      directory: FilesystemDirectory.Documents,
      encoding: FilesystemEncoding.UTF8
    });
    const parsed = JSON.parse(contents.data);
    return parsed;
  } catch (e) {
    console.error("Unable to read file", e);
  }
}

export async function addToDownloadQueue(item) {
  try {
    let ret = await Filesystem.stat({
      path: filename,
      directory: FilesystemDirectory.Documents
    });
    console.log("ret", ret);
    const data = await fileRead();

    console.log("data", data);
    const itemAlreadyInQueue = data.find(ele => ele.id === item.id);
    // if (itemAlreadyInQueue) {
    //   alert("Already in download queue");
    //   return;
    // } else {
    data.push(item);
    await fileWrite(data);
    console.log("Written");
    StartDownloadQueue();
    // }
  } catch (e) {
    console.error("Unable to get status of file", e);
    fileWrite([item]);
  }
}
export async function fileDelete() {
  await Filesystem.deleteFile({
    path: filename,
    directory: FilesystemDirectory.Documents
  });
}
export async function stat() {}
