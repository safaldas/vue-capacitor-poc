import Vue from "vue";
import Router from "vue-router";
// import CameraPage from "@/components/CameraPage";
import GeolocationPage from "@/components/GeolocationPage";
import Videos from "@/components/videos";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "Videos",
      component: Videos
    },
    {
      path: "/geo-location-page",
      name: "GeolocationPage",
      component: GeolocationPage
    }
  ]
});
