import { fileRead } from "./fileSystemHelper";
import { Plugins } from "@capacitor/core";
let downloadProcessRunning = false;
export async function StartDownloadQueue() {
  if (!downloadProcessRunning) {
    const data = await fileRead();
    if (data.length) {
      startAsyncDownload();
    }
  }
}

async function startAsyncDownload() {
  const { Filesystem } = Plugins;

  const complete = () => {
    console.log("Done");
  };
  const error = err => {
    console.log("Download failed: " + err);
  };
  const progress = progress => {
    console.log(
      ((100 * progress.bytesReceived) / progress.totalBytesToReceive).toFixed(
        2
      ) + "%"
    );
  };

  try {
    const fileName = "file_example_MP4.mp4";
    const downloadUrl =
      "https://projectskinmd.com/wp-content/uploads/project-skin/fullscreen/large/1317158452.mp4";

    const { uri: filePath } = await Filesystem.getUri({
      directory: "DATA",
      path: fileName
    });
    const targetFile = wrapTargetFile(filePath);

    const downloader = new BackgroundTransfer.BackgroundDownloader();
    // Create a new download operation.
    const download = downloader.createDownload(downloadUrl, targetFile);
    // Start the download and persist the promise to be able to cancel the download.
    download.startAsync().then(complete, error, progress);
  } catch (err) {
    console.error("Error: " + err);
    alert(JSON.stringify(err));
  }
}
const wrapTargetFile = filePath => ({ toURL: () => filePath });
